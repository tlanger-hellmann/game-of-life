package gol;

import static java.util.function.Predicate.not;

import java.awt.Point;
import java.awt.Rectangle;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Field {

	Set<Point> living = new HashSet<>();

	void set(int x, int y) {
		living.add(new Point(x, y));
	}

	Stream<Point> block9(Point p) {
		return Stream.of(p.x - 1, p.x, p.x + 1).flatMap(
				xn -> Stream.of(p.y - 1, p.y, p.y + 1).map(
						yn -> new Point(xn, yn))
		);
	}

	boolean willLive(Point p) {
		long neigbors = block9(p)
				.filter(not(p::equals))
				.filter(living::contains)
				.count();
		if (living.contains(p)) {
			return neigbors == 2 || neigbors == 3;
		} else {
			return neigbors == 3;
		}
	}

	void tick() {
		living = living.stream()
				.flatMap(this::block9)
				.distinct()
				.filter(this::willLive)
				.collect(Collectors.toSet());
	}

	// ---   ugly dump code follows here:

	void dump() {
		var dim = living.stream().collect(Rectangle::new, Rectangle::add, Rectangle::add);
		IntStream.range(dim.y - 1, dim.y + dim.height + 2).forEach(
				y -> {
					IntStream.range(dim.x - 1, dim.x + dim.width + 2).forEach(
							x -> {
								if (living.contains(new Point(x, y))) {
									System.out.print("X");
								} else {
									System.out.print(" ");
								}
							}
					);
					System.out.println();
				}
		);
		System.out.println();
		for (int x = 0; x <= dim.width + 5; x++) {
			System.out.print("-");
		}
		System.out.println();
		System.out.println();
	}

}
