package gol;

import java.util.Random;
import java.util.stream.IntStream;

public class Main {

	static Random random = new Random();

	public static void main(String[] args) throws InterruptedException {
		Field field = new Field();
		IntStream.range(0, 150).forEach(x ->
				field.set(random.nextInt(30), random.nextInt(30)));
		do {
			field.dump();
			field.tick();
			Thread.sleep(200);
		} while (true);
	}
}
